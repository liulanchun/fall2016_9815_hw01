# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 15:17:33 2016

@author: liu
"""

# Mortgage amortization

from decimal import *
import numpy as np
from collections import OrderedDict
import math

PRINCIPAL = 500000 # principal amount
RATE = 3.875/100. # interest rate
TERM = 360 # number of payments (months)
EXTRA_PAYMENT = 0 # extra amount of principal every month


YC = { 1: 0.73818, 2: 0.83103, 3: 0.85393, 4: 0.87344,
       5: 0.89156, 6: 0.90619, 9: 0.94035, 12: 0.96712,
      18: 1.01837, 24: 1.05664212, 36: 1.11874651,
      48: 1.17941657, 60: 1.23843929, 72: 1.30179396,
      84: 1.36339275, 96: 1.42007995, 108: 1.47176883,
     120: 1.51815895, 144: 1.60114194, 180: 1.69000072,
     240: 1.78308961, 300: 1.82618131, 360: 1.84928439 }


    
def amortization_table(principal, rate, term, extraPay):
    ''' Prints the amortization table for a loan '''

    ttl = [0,0,0] # totals to be displayed at the end

    payment = pmt(principal, rate, term)  + extraPay
    begBal = principal
    rates = getRates()
    mtgVal = 0.0
    
    # Print headers
    print 'Pmt no'.center(6), ' ',
    print 'Payment'.ljust(9), ' ', 'Principal'.ljust(9), ' ',
    print 'Interest'.ljust(9), ' ', 'End. bal.'.ljust(13)
    print ''.rjust(6, '-'), ' ', ''.ljust(9, '-'), ' ',
    print ''.rjust(9, '-'), ' ', ''.ljust(9, '-'), ' ',
    print ''.rjust(9, '-'), ' '
    # Print data
    for num in range(1, term + 1):
        
#        interest = round(begBal * (rate / (12 * 100.0)), 2)
        principal = 0
#        interest = 0 
        interest = begBal * (rate / 12.)
        begBal += interest
        if begBal >= payment:
           principal = payment - interest
           endBal = begBal - payment
        
        if begBal > 0 and begBal <payment :
           payment = begBal
           principal = payment - interest
           endBal = 0

        if begBal <= 0 :
           begBal = 0 
           payment = 0
           interest = 0
           principal = 0
           endBal = 0

        #discount each payment based on YC to calculate the total value of the mortgage
        mtgVal += payment * discountFactor(num, rates[num-1])

           
        print str(num).center(6), ' ',
 #       print '{0:,.2f}'.format(begBal).rjust(13), ' ',
        print '{0:,.2f}'.format(payment).rjust(9), ' ',
        print '{0:,.2f}'.format(principal).rjust(9), ' ',
        print '{0:,.2f}'.format(interest).rjust(9), ' ',
        print '{0:,.2f}'.format(endBal).rjust(9)

        ttl[0] = ttl[0] + payment
        ttl[1] = ttl[1] + principal
        ttl[2] = ttl[2] + interest
        begBal = endBal

    print "Total".center(6), ' ',
 #   print " ".rjust(13), ' ',
    print '{0:,.2f}'.format(ttl[0]).rjust(9), ' ',
    print '{0:,.2f}'.format(ttl[1]).rjust(9), ' ',
    print '{0:,.2f}'.format(ttl[2]).rjust(9), ' ',
    print " ".rjust(13)

    print "total value of mortgage is " + str(mtgVal)    

def pmt(principal, rate, term):
    '''Calculates the payment on a loan.

    Returns the payment amount on a loan given
    the principal, the interest rate (as an APR),
    and the term (in months).'''
    
    ratePerTwelve = rate / (12.0)    
    result = principal * (ratePerTwelve / (1 - (1 + ratePerTwelve) ** (-term)))

    return result

def getRates():
   ''' use numpy interpolation to calculate rates for all terms '''
   ordYC=OrderedDict()
   for key in sorted(YC):
       ordYC[key] = YC.get(key)
       
   return np.interp(range(1  ,TERM+1), ordYC.keys(), ordYC.values() )
    
def discountFactor(term, rate):
    ''' semi-annual compounding  '''   
    return math.pow(1+rate/200.,  -1.0*term/6.0)    

if __name__ == '__main__':
   #problem 1 & 2a    
   print "P1 & 2a "
   amortization_table(PRINCIPAL, RATE, TERM,EXTRA_PAYMENT)
   
   
   print "\nProblem 2b"
   extrapay =  pmt(PRINCIPAL, RATE, TERM)/12.0   
   amortization_table(PRINCIPAL, RATE, TERM,extrapay)
   